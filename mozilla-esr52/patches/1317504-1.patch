
# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1479967621 -32400
# Node ID cffe1cd111f65c9045cc8e1fe34d845a9a6b5c28
# Parent  5c3271cb35c218a5976aa87873099b45719178ea
Bug 1317504 - Warn that LD is not used by the build system. r=chmanchester, a=IanN CLOSED TREE DONTBUILD
mozilla-esr52 SEAMONKEY_2_49_ESR_RELBRANCH

It turns out that, in practice, the LD variable is not used by the build
system, except on Windows, where it's used to feed the default for LINK,
which is then re-injected as LD.

The upcoming changes are going to normalize the use of LD/LINK.

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -791,16 +791,40 @@ def compiler(language, host_or_target, c
         }[language]
 
         preprocessor = depends_if(valid_compiler)(
                 lambda x: list(x.wrapper) + [x.compiler, '-E'] + list(x.flags))
 
         set_config(pp_var, preprocessor)
         add_old_configure_assignment(pp_var, preprocessor)
 
+    if language == 'C':
+        linker_var = {
+            target: 'LD',
+            host: 'HOST_LD',
+        }[host_or_target]
+
+        @deprecated_option(env=linker_var, nargs=1)
+        def linker(value):
+            if value:
+                return value[0]
+
+        @depends(valid_compiler, linker)
+        def unused_linker(compiler, linker):
+            if linker and compiler.type != 'msvc':
+                log.warning('The value of %s is not used by this build system.'
+                            % linker_var)
+
+        if host_or_target == target:
+            @depends(valid_compiler)
+            def is_msvc(compiler):
+                return compiler.type == 'msvc'
+
+            imply_option('LINK', linker, reason='LD', when=is_msvc)
+
     return valid_compiler
 
 
 c_compiler = compiler('C', target)
 cxx_compiler = compiler('C++', target, c_compiler=c_compiler)
 host_c_compiler = compiler('C', host, other_compiler=c_compiler)
 host_cxx_compiler = compiler('C++', host, c_compiler=host_c_compiler,
                              other_compiler=cxx_compiler,
diff --git a/build/moz.configure/windows.configure b/build/moz.configure/windows.configure
--- a/build/moz.configure/windows.configure
+++ b/build/moz.configure/windows.configure
@@ -405,21 +405,17 @@ def valid_mt(path):
     except subprocess.CalledProcessError:
         pass
     raise FatalCheckError('%s is not Microsoft Manifest Tool')
 
 
 set_config('MSMANIFEST_TOOL', depends(valid_mt)(lambda x: bool(x)))
 
 
-# Ultimately, this will move to toolchain.configure and be turned into a
-# cross-platform check.
-option(env='LD', nargs=1, help='Path to the linker')
-
-link = check_prog('LINK', depends_win()(lambda: ('link.exe',)), input='LD',
+link = check_prog('LINK', depends_win()(lambda: ('link.exe',)),
                   paths=vc_compiler_path)
 
 add_old_configure_assignment('LD', depends_win(link)(lambda x: x))
 
 
 # Normally, we'd just have CC, etc. set to absolute paths, but the build system
 # doesn't currently handle properly the case where the paths contain spaces.
 # Additionally, there's the issue described in toolchain.configure, in
diff --git a/python/mozbuild/mozbuild/test/configure/test_moz_configure.py b/python/mozbuild/mozbuild/test/configure/test_moz_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_moz_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_moz_configure.py
@@ -14,16 +14,20 @@ class TestMozConfigure(BaseConfigureTest
     def test_moz_configure_options(self):
         def get_value_for(args=[], environ={}, mozconfig=''):
             sandbox = self.get_sandbox({}, {}, args, environ, mozconfig)
 
             # Add a fake old-configure option
             sandbox.option_impl('--with-foo', nargs='*',
                                 help='Help missing for old configure options')
 
+            # Remove all implied options, otherwise, getting
+            # all_configure_options below triggers them, and that triggers
+            # configure parts that aren't expected to run during this test.
+            del sandbox._implied_options[:]
             result = sandbox._value_for(sandbox['all_configure_options'])
             shell = mozpath.abspath('/bin/sh')
             return result.replace('CONFIG_SHELL=%s ' % shell, '')
 
         self.assertEquals('--enable-application=browser',
                           get_value_for(['--enable-application=browser']))
 
         self.assertEquals('--enable-application=browser '

